# NLP_Test_Prosa-MFeriandaS

This project was created to complete the task of recruitment of NLP AI Engineer, prosa.ai, made by Muhammad ferianda satya.


## prerequisites
Before running the program or train the model, please install python library down bellow. make sure you have python 3 installed, recomended using anaconda.
```
pip install flask
pip install pandas
pip install nltk
pip install matplotlib
pip install sastrawi
pip install textblob
pip install re
pip install tensorfloe
pop install numpy
```

## Running the program
If you want to re train the model you can just open Model_Training.ipynb, this file already using cleaned dataset

## Result
This model is made by applying LSTM architecture using keras. 

![Model network](/images/Model Network.png)

With this model, data is trained using adam optimizer using lr = 000.4, validation split = 0.2 during training and 50 epoch.

The result of the model are pretty good, can be look at this history plot for accuracy and loss during the training phase.

![Model plot](/images/plot.png)

This model get testing accuracy >70% from the test set.

## Predict new comment using request
Run app.py by running this script in terminal / cmd 
```
python app.py
```
Then open yout browser and open this link
```
http://0.0.0.0:105/request/"Your comment"
```
The result will appear in Json Format.

## License
Dataset is owned by PT Prosa Solusi Cerdas. 
This dataset is used for assignment only
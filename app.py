from flask import Flask
import json
import pandas as pd
import numpy as np 

from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
import re 
import string

app = Flask(__name__)

def preproccess(df):
    # Lowering all words
    df = df.apply(lambda x: x.lower())
    # remove all numbers
    df = df.apply(lambda x: re.sub(r"\d+", "", x))
    # remove all punctuations
    df = df.apply(lambda x: x.translate(str.maketrans("","",string.punctuation)))
    # remove whitespaces
    df = df.apply(lambda x: x.strip())
    
    #stop word removal
    factory = StopWordRemoverFactory()
    stopword = factory.create_stop_word_remover()
    df = df.apply(lambda x: stopword.remove(x))
    
    # Stemming, this may take a while
    factory = StemmerFactory()
    stemmer = factory.create_stemmer()
    df = df.apply(lambda x: stemmer.stem(x))

    return df

@app.route('/request/<string:sentence>/')
def incrementer(sentence):
    #read train set
    train_set = pd.read_pickle("ProccessedData/Preproccessed_data_train.pkl")
    
    from tensorflow.keras.models import load_model
    # load model from single file
    model = load_model('saved_model/lstm_model.h5')

    df = pd.DataFrame([sentence])
    cleaned = preproccess(df[0])
    tokenizer = Tokenizer(num_words=5340, split=' ')
    tokenizer.fit_on_texts(train_set['Customer Voice'])

    X1 = tokenizer.texts_to_sequences(cleaned)
    X = pad_sequences(X1, maxlen=65) 

    pred = model.predict_classes(X)[0]
    if str(pred[0]) == '0':
        data = {'Sentence': sentence,
                            'Cleaned Sentence': cleaned[0],
                            'Label': 'Negative'}
    else:
        data = {'Sentence': sentence,
                            'Cleaned Sentence': cleaned[0],
                            'Label': 'Positive'}
    # data = make_summary()
    response = app.response_class(
        response=json.dumps(data, indent=4),
        status=200,
        mimetype='application/json'
    )

    return response

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=105)